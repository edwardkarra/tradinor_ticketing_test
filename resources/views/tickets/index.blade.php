@extends('layouts.app')

@section('content')
    @vite(['resources/css/tickets/index.css'])

    <div class="container">
        <h1>Ticket List</h1>

        <div class="mb-3">
            <form action="{{ route('tickets.index', request()->query()) }}" method="GET" class="form-inline">
                <input type="text" name="search" class="form-control mr-sm-2" placeholder="Search">
                @foreach (request()->query() as $k => $field)
                    <input hidden type="text" name="{{ $k }}" class="form-control mr-sm-2"
                        value="{{ $field }}">
                @endforeach
                <button type="submit" class="btn btn-primary">Search</button>
            </form>
        </div>

        <table class="table">
            <thead>
                <tr>
                    <th><a
                            href="{{ route('tickets.index', ['sort' => 'title', 'direction' => request()->sort == 'title' ? (request()->direction *= -1) : 1] + request()->query()) }}">Title</a>
                    </th>
                    <th><a
                            href="{{ route('tickets.index', ['sort' => 'name', 'direction' => request()->sort == 'name' ? (request()->direction *= -1) : 1] + request()->query()) }}">Name</a>
                    </th>
                    <th><a
                            href="{{ route('tickets.index', ['sort' => 'created_at', 'direction' => request()->sort == 'created_at' ? (request()->direction *= -1) : 1] + request()->query()) }}">Date</a>
                    </th>
                    <th><a
                            href="{{ route('tickets.index', ['sort' => 'importance', 'direction' => request()->sort == 'importance' ? (request()->direction *= -1) : 1] + request()->query()) }}">Importance</a>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($tickets as $k => $ticket)
                    <div>
                        <tr class="hover-tr" id="row-{{ $k }}">
                            <td>{{ $ticket->title }}</td>
                            <td>{{ $ticket->name }}</td>
                            <td>{{ $ticket->created_at }}</td>
                            <td class="{{ getImportanceBadgeClass($ticket->importance) }}">{{ $ticket->importance }}</td>
                        </tr>
                    </div>
                @endforeach
            </tbody>
        </table>
        <script>
            @foreach ($tickets as $k => $ticket)
                const row{{ $k }} = document.getElementById(`row-{{ $k }}`);
                row{{ $k }}.addEventListener("click", () => {
                    window.location.href = '{{ route('tickets.show', ['ticket' => $ticket->id]) }}';
                });
            @endforeach
        </script>

        <div class="pagination">
            {{ $tickets->links() }}
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
@endsection
