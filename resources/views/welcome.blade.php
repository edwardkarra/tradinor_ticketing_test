@extends('layouts.app')
@if (auth()->check())
    <script>
        window.location.href = "{{ route('home') }}";
    </script>
@endif
@section('content')
        <section class="section section-dark">
            <div class="container">
                <h1>Welcome to the Ticket System</h1>
                <p class="lead">Get quick and efficient support for your inquiries</p>
            </div>
        </section>
    
        <section class="section section-light">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2>Already have an account?</h2>
                        <p>Log in to access the ticket system and manage your inquiries.</p>
                        <a class="btn btn-primary" href="{{ route('login') }}">Login</a>
                    </div>
                    <div class="col-md-6">
                        <h2>New user?</h2>
                        <p>Create an account to start filing tickets and get support.</p>
                        <a class="btn btn-secondary" href="{{ route('register') }}">Register</a>
                    </div>
                </div>
            </div>
        </section>
    
        <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>

@endsection