<!DOCTYPE html>
<html>
<head>
    <style>
        /* Styles for the email template */
        body {
            font-family: Arial, sans-serif;
            background-color: #f1f1f1;
        }
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0 2px 4px rgba(0,0,0,0.1);
        }
        h1 {
            color: #333;
        }
        p {
            margin-bottom: 20px;
        }
        .ticket-details {
            background-color: #f9f9f9;
            border-radius: 5px;
            padding: 10px;
        }
        .ticket-details p {
            margin: 0;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>New Ticket Submitted</h1>
        <p>Hello Admin,</p>
        <p>A new ticket has been submitted. Here are the details:</p>

        <div class="ticket-details">
            <p><strong>Name:</strong> {{ $ticket->name }}</p>
            <p><strong>Email:</strong> {{ $ticket->email }}</p>
            <p><strong>Phone Number:</strong> {{ $ticket->phone_number }}</p>
            <p><strong>Message:</strong></p>
            <p>{{ $ticket->description }}</p>
        </div>

        <p>Thank you for your attention.</p>
    </div>
</body>
</html>