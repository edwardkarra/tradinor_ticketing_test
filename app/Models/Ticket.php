<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Storage;

class Ticket extends Model
{
    use HasFactory;
    public $fillable = [
        'title',
        'description',
        'attachment',
        'importance',
        'name',
        'email',
        'phone_number',
    ];

    public $hidden = [
        'updated_at',
        'user_id',
    ];

    public function statusHistory(){
        return $this->hasMany(TicketStatusHistory::class);
    }

    public function comments(){
        return $this->hasMany(TicketComment::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function getAttachmentUrlAttribute(){
        // create a temp token to secure the download request
        $token = auth()->user()->createToken('storage_access', [], now()->addMinutes(30))->plainTextToken;
        return Storage::disk('private')->url($this->getAttribute('attachment')."?token=$token");
    }
}