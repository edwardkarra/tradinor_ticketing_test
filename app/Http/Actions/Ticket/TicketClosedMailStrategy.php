<?php
namespace App\Http\Actions\Ticket;
use App\Http\Actions\ActionInterface;
use App\Mail\TicketClosedMail;
use App\Mail\TicketStoreMail;
use App\Models\Ticket;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;
class TicketClosedMailStrategy extends TicketClosedNotifyStrategy{
    private Ticket $ticket;

    public function __construct(Ticket $ticket){
        $this->ticket = $ticket;
    }
    public function perform(){
        Mail::to($this->ticket->email)->send(new TicketClosedMail($this->ticket));
    }
}