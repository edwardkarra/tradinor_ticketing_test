@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="card mb-3">
                    <div class="card-header">
                        <h5 class="card-title">Ticket Details</h5>
                    </div>
                    <div class="card-body">
                        <p><strong>Title:</strong> {{ $ticket->title }}</p>
                        <p><strong>Description:</strong> {{ $ticket->description }}</p>
                        <p><strong>Attachment:</strong>
                            @if ($ticket->attachment)
                                <a href="{{ $ticket->attachment_url }}" download>Download Attachment</a>
                            @else
                                No attachment
                            @endif
                        </p>
                        <p><strong>Importance :</strong> <span
                                class="badge {{ getImportanceBadgeClass($ticket->importance) }}">{{ Str::upper($ticket->importance) }}</span>
                        </p>
                        <p><strong>Status:</strong> <span
                                class="badge {{ getStatusBadgeClass($ticket->status) }}">{{ getStatusText($ticket->status) }}</span>
                        </p>
                        @if (session('success'))
                            <div class="alert alert-success">
                                Status changes successfully!
                            </div>
                        @endif
                        <form id="change-status-form"
                            action='{{ route('tickets.change_status', ['ticket' => $ticket->id]) }}' method="POST">
                            @csrf
                            <input type="hidden" name="redirect"
                                value="{{ route('tickets.show', ['ticket' => $ticket->id]) }}">
                            <input hidden type="text" name="_method" value="PUT">
                            <select {{!auth()->user()->hasRole('admin') ? 'disabled' : ''}} name="new_status" class="form-select col-md-6" aria-label="Change status"
                                onchange="changeStatus()">
                                <option {{ $ticket->status == 'pending' ? 'selected' : '' }} value="pending">Pending
                                </option>
                                <option {{ $ticket->status == 'in_progress' ? 'selected' : '' }} value="in_progress">In
                                    progress</option>
                                <option {{ $ticket->status == 'closed' ? 'selected' : '' }} value="closed">Closed</option>
                            </select>
                        </form>
                        <br />
                        @foreach ($status_history as $status)
                            <p>
                                Status changed from
                                <span
                                    class="{{ getStatusBadgeClass($status->old_status) }}">{{ getStatusText($status->old_status) }}</span>
                                to
                                <span
                                    class="{{ getStatusBadgeClass($status->new_status) }}">{{ getStatusText($status->new_status) }}</span>
                                on {{ $status->created_at->format('Y-m-d H:i:s') }}
                            </p>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Comments</h5>
                    </div>
                    <div class="card-body">
                        <ul class="list-group">
                            @foreach ($comments->reverse() as $comment)
                                <li class="list-group-item">
                                    <h6>{{ $comment->user->name }} | {{ $comment->created_at }}</h6>
                                    <p>{{ $comment->body }}</p>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    @if(auth()->user()->hasRole('admin'))
                    <div class="card-footer">
                        <form action="{{ route('comments.store', ['ticket' => $ticket->id]) }}" method="POST">
                            @csrf
                            <input type="hidden" name="redirect"
                                value="{{ route('tickets.show', ['ticket' => $ticket->id]) }}">
                            <div class="form-group">
                                <textarea class="form-control" name="body" rows="3" placeholder="Add a comment"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Add Comment</button>
                        </form>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <script>
        function changeStatus() {
            document.getElementById("change-status-form").submit();
        }
    </script>
@endsection
