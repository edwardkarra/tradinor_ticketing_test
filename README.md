# Ticketing System "Tradinos"

Welcome to the Backend Engineer Technical Test for the Ticketing System project. This project aims to build a ticket management system where users can create tickets, track their status, and admins can manage and respond to those tickets. This README.md file provides an overview of the project and instructions for running it.

## Project Description

The Ticketing System allows users to create new tickets by providing details such as title, description, attachment, name, email, phone number, and importance level (Urgent, Important, or Normal). After submitting the ticket, users will see a thank you message with a link to check the ticket status and response (accessible only by the user). An email notification will also be sent to the admin with the ticket description.

On the admin side, admins can log in to view the tickets. They can add comments to the tickets, change the status of tickets (Pending, In Progress, or Closed). When the status is set to Closed, an email will be sent to the user's email address with a link to review the ticket.

Admins can search for tickets and sort them by date (newest to oldest) or importance of the tasks (high to low importance).

In addition, the project includes building a REST API that can be utilized in the future to develop a mobile app for the admin. The mobile app would include functionality for login, browsing tickets with search and sort capabilities, and changing ticket status.

## Notes

1. The project is built using the Laravel framework.
1. Email notification is currently implemented, but in the future, SMS providers can be integrated.

## Deployment Process

To deploy the Ticketing System, follow these steps:

1. Copy the `.env.example` file and rename it to `.env`.
1. Set up the database configuration and mail provider in the `.env` file.
1. Update the `APP_URL` value in the `.env` file to match the URL of your application.
1. Use the `master` branch of the codebase.
1. Run the following commands in the project root directory:

```bash
composer install
php artisan migrate
php artisan db:seed
npm install && npm run build
php artisan serve

seeded admin:
email: admin@example.com
password: password
```

6. After running the above commands, the Ticketing System should be up and running. Visit the specified URL to access the application.

Please note that these instructions assume a basic setup for a local development environment. Adjustments may be needed based on your specific deployment environment.