<?php
namespace App\Http\Actions\Ticket;
use App\Http\Actions\ActionInterface;
use App\Mail\TicketStoreMail;
use App\Models\Ticket;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;
abstract class TicketStoreNotifyStrategy implements ActionInterface{
    private Ticket $ticket;
    public function __construct(Ticket $ticket){
        $this->ticket = $ticket;
    }
    public function perform(){}
}