<?php
namespace App\Http\Actions\Ticket;
use App\Http\Actions\ActionInterface;
use App\Mail\TicketStoreMail;
use App\Models\Ticket;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;
class TicketClosedNotifyAction implements ActionInterface{
    private TicketClosedNotifyStrategy $strategy;
    public function __construct(TicketClosedNotifyStrategy $strategy){
        $this->strategy = $strategy;
    }

    public static function make(TicketClosedNotifyStrategy $strategy){
        return new self($strategy);
    }
    public function perform(){
        $this->strategy->perform();
    }
}