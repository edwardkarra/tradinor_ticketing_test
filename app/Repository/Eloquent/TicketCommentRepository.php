<?php

namespace App\Repository\Eloquent;

use App\Models\Ticket;
use App\Models\TicketComment;
use App\Repository\TicketCommentRepositoryInterface;
use App\Repository\TicketRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class TicketCommentRepository extends BaseRepository implements TicketCommentRepositoryInterface
{

    /**
     * TicketCommentRepository constructor.
     *
     * @param TicketComment $model
     */
    public function __construct(TicketComment $model)
    {
        parent::__construct($model);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->model->all();
    }

    /**
     * @return TicketComment
     */
    public function createComment(Ticket $ticket, array $fields) : TicketComment{
        $fields['user_id'] = Auth::id();
        return $ticket->comments()->create($fields);
    }

    /**
     * @return Collection
     */
    public function getOfTicket(Ticket $ticket) : Collection{
        return $ticket->comments()->latest()->limit(10)->get();
    }

}