<!DOCTYPE html>
<html>
<head>
    <style>
        /* Styles for the email template */
        body {
            font-family: Arial, sans-serif;
            background-color: #f1f1f1;
        }
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0 2px 4px rgba(0,0,0,0.1);
        }
        h1 {
            color: #333;
        }
        p {
            margin-bottom: 20px;
        }
        .ticket-details {
            background-color: #f9f9f9;
            border-radius: 5px;
            padding: 10px;
        }
        .ticket-details p {
            margin: 0;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Your Ticket Is Closed</h1>
        <p>Hello {{$ticket->name}},</p>
        <p>Your ticket "{{$ticket->title}}" is closed. Here are the details:</p>

        <div class="ticket-details">
            <p><strong>Title:</strong> {{ $ticket->title }}</p>
            <p><strong>Message:</strong></p>
            <p>{{ $ticket->description }}</p>
            <hr/>
            <p>for more info please visit this link:</p>
            <p><strong>Ticket link:</strong> {{route('tickets.show', ['ticket' => $ticket->id])}}</p>
        </div>

        <p>Thank you for your attention.</p>
    </div>
</body>
</html>