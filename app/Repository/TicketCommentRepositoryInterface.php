<?php
namespace App\Repository;

use App\Models\Ticket;
use App\Models\TicketComment;
use Illuminate\Support\Collection;

interface TicketCommentRepositoryInterface
{
   public function all(): Collection;
   public function createComment(Ticket $ticket, array $fields) : TicketComment;
   public function getOfTicket(Ticket $ticket) : Collection;
}