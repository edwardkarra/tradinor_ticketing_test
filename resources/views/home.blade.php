@extends('layouts.app')
@if (auth()->check() &&
        !auth()->user()->hasRole('user'))
    <script>
        window.location.href = "{{ route('tickets.index') }}";
    </script>
@endif

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Ticket Submission</div>
                    <div class="card-body">Fill this form to file a ticket to our system, we will be in touch soon!</div>
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                        <div class="alert alert-info">
                            This is the link for tracking the ticket
                            <a href="{{ session('link') }}">
                                {{ session('link') }}
                            </a>
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }} is not valid</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card-body">
                        <form action="{{ route('tickets.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" id="title" name="title" required>
                            </div>
                            
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" id="description" name="description" required></textarea>
                            </div>
                            
                            <div class="mb-3">
                                <label for="attachment" class="form-label">Attachment</label>
                                <input class="form-control" type="file" id="attachment" name="attachment">
                            </div>
                            <div class="form-group">
                                <label for="importance">Task Importance</label>
                                <select class="form-control" id="importance" name="importance" required>
                                    <option value="Urgent">Urgent</option>
                                    <option value="Important">Important</option>
                                    <option value="Normal">Normal</option>
                                </select>
                            </div>
                            <hr/>
                            <p class="fst-italic"> * Information of the ticket owner (initially the current user)</p>

                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name" required
                                    value="{{ auth()->user()->name }}">
                            </div>

                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email" required
                                    value="{{ auth()->user()->email }}">
                            </div>

                            <div class="form-group">
                                <label for="phone">Phone Number</label>
                                <input type="tel" class="form-control" id="phone" name="phone_number" required>
                            </div>
                            <input type="hidden" name="redirect" value="{{ route('home') }}">

                            <button type="submit" class="btn btn-primary">Submit Ticket</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
