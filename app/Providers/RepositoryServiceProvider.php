<?php

namespace App\Providers;

use App\Repository\Eloquent\BaseRepository;
use App\Repository\Eloquent\TicketCommentRepository;
use App\Repository\Eloquent\TicketRepository;
use App\Repository\Eloquent\TStatusHistoryRepository;
use App\Repository\EloquentRepositoryInterface;
use App\Repository\TicketCommentRepositoryInterface;
use App\Repository\TicketRepositoryInterface;
use App\Repository\TStatusHistoryRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EloquentRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(TicketRepositoryInterface::class, TicketRepository::class);
        $this->app->bind(TStatusHistoryRepositoryInterface::class, TStatusHistoryRepository::class);
        $this->app->bind(TicketCommentRepositoryInterface::class, TicketCommentRepository::class);
    }
}