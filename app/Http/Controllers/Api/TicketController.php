<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\TicketChangeStatusRequest;
use App\Http\Requests\TicketShowRequest;
use App\Http\Requests\TicketStoreRequest;
use App\Http\Resources\TicketResource;
use App\Mail\TicketClosedMail;
use App\Models\Ticket;
use App\Repository\TicketCommentRepositoryInterface;
use App\Repository\TicketRepositoryInterface;
use App\Repository\TStatusHistoryRepositoryInterface;
use Illuminate\Http\Request;
use Mail;

class TicketController extends Controller
{
    private $ticketRepository;
    private $statusHistoryRepository;
    private $commentRepository;
    public function __construct(TicketRepositoryInterface $ticketRepository,TStatusHistoryRepositoryInterface $statusHistoryRepository, TicketCommentRepositoryInterface $commentRepository) {
        $this->ticketRepository = $ticketRepository;
        $this->statusHistoryRepository = $statusHistoryRepository;
        $this->commentRepository = $commentRepository;
    }

    public function index(Request $request)
    {
        $search = $request->input('search');
        $sort = $request->input('sort', 'created_at');
        $direction = $request->input('direction', -1);
        $tickets = $this->ticketRepository->getPaginatedTickets($search, $sort, $direction);

        return apiResponse(
            TicketResource::collection($tickets),
            null,
            200,
            $tickets
        );
    }

    public function changeStatus(TicketChangeStatusRequest $request, Ticket $ticket){
        $old_status = $ticket->status;
        $this->ticketRepository->changeStatus($ticket, $request->validated()['new_status']);
        $this->ticketRepository->createHistoryRecord($ticket->refresh(), $old_status);

        $ticket->status == 'closed' ? Mail::to($ticket->email)->send(new TicketClosedMail($ticket)) : null;
        
        return apiResponse(
            TicketResource::make($ticket),
            "Status changed successfully!",
            200,
        );
    } 
}