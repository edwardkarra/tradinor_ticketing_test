<?php
namespace App\Repository;

use App\Models\Ticket;
use App\Models\TicketStatusHistory;
use Illuminate\Support\Collection;

interface TStatusHistoryRepositoryInterface
{
   public function getOfTicket(Ticket $ticket): Collection;
}