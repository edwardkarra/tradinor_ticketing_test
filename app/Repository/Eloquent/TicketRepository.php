<?php

namespace App\Repository\Eloquent;

use App\Models\Ticket;
use App\Repository\TicketRepositoryInterface;
use Illuminate\Http\UploadedFile;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class TicketRepository extends BaseRepository implements TicketRepositoryInterface
{

    /**
     * TicketRepository constructor.
     *
     * @param Ticket $model
     */
    public function __construct(Ticket $model)
    {
        parent::__construct($model);
    }

    public function getPaginatedTickets($search = null, $sort = 'date', $direction = -1): LengthAwarePaginator
    {
        $query = Ticket::query();

        if ($search) {
            $query->where('title', 'like', '%' . $search . '%')
                ->orWhere('name', 'like', '%' . $search . '%')
                ->orWhere('phone_number', 'like', '%' . $search . '%')
                ->orWhere('email', 'like', '%' . $search . '%');
        }
        $direction = $direction == 1 ? 'asc' : 'desc';
        switch ($sort) {
            case 'created_at':
                $query->orderBy('created_at', $direction);
                break;
            case 'title':
                $query->orderBy('title', $direction);
                break;
            case 'name':
                $query->orderBy('name', $direction);
                break;
            case 'importance':
                $query->orderByRaw("FIELD(importance, 'normal', 'important', 'urgent') $direction");
                break;
        }

        return $query->paginate(request()->items_per_page ?: 10);
    }

    public function createTicket(array $fields): Ticket
    {
        if (isset($fields['attachment']) && is_file($fields['attachment'])) {
            $fields['attachment'] = Storage::disk('private')->putFileAs('/ticket_attachments', $fields['attachment'], uniqid('att_', false).'.'.$fields['attachment']->getClientOriginalExtension());
        }
        $fields['importance'] = \Str::lower($fields['importance']);
        $ticket = Auth::user()->tickets()->create($fields);
        return $ticket;
    }

    public function createHistoryRecord(Ticket $ticket, string $old_status): Ticket
    {
        $ticket->statusHistory()->create([
            'old_status' => \Str::lower($old_status),
            'new_status' => $ticket->status,
        ]);
        return $ticket;
    }
    public function changeStatus(Ticket $ticket, string $new_status): Ticket
    {
        $ticket->status = $new_status;
        $ticket->save();
        return $ticket;
    }

}