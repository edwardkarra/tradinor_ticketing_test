<?php

namespace App\Http\Controllers;

use App\Http\Requests\TicketCommentStoreRequest;
use App\Models\Ticket;
use App\Repository\TicketCommentRepositoryInterface;
use Illuminate\Http\Request;

class TicketCommentController extends Controller
{
    private $commentRepository;
    /**
     * Construct the controller
     */
    public function __construct(TicketCommentRepositoryInterface $commentRepository){
        $this->commentRepository = $commentRepository;
    }

    /**
     * Store a comment
     */
    public function store(TicketCommentStoreRequest $request, Ticket $ticket){
        $this->commentRepository->createComment($ticket, $request->validated());
        return redirect($request->input('redirect'));
    }
}
