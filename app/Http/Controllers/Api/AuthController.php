<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ApiLoginRequest;
use Auth;
class AuthController extends Controller
{
    public function login(ApiLoginRequest $request){
        if (Auth::attempt($request->validated())) {
            $user = Auth::user();
            $token = $user->createToken('api_token')->plainTextToken;
    
            return apiResponse([
                'token' => $token
            ], "logged in successfully!");
        }
    
        return apiResponse(null, "Invalid Credentials", 401);
    }
}
