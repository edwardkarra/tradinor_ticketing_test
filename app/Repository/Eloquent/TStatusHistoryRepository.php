<?php

namespace App\Repository\Eloquent;

use App\Models\Ticket;
use App\Models\TicketStatusHistory;
use App\Repository\TStatusHistoryRepositoryInterface;
use Illuminate\Support\Collection;

class TStatusHistoryRepository extends BaseRepository implements TStatusHistoryRepositoryInterface
{

    /**
     * TicketRepository constructor.
     *
     * @param TicketStatusHistory $model
     */
    public function __construct(TicketStatusHistory $model)
    {
        parent::__construct($model);
    }

    /**
     * @return Collection
     */
    public function getOfTicket(Ticket $ticket): Collection
    {
        return $ticket->statusHistory()->latest()->limit(10)->get();
    }

    
}