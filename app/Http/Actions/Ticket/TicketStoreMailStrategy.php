<?php
namespace App\Http\Actions\Ticket;
use App\Http\Actions\ActionInterface;
use App\Mail\TicketStoreMail;
use App\Models\Ticket;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;
class TicketStoreMailStrategy extends TicketStoreNotifyStrategy{
    private Ticket $ticket;
    public function __construct(Ticket $ticket){
        $this->ticket = $ticket;
    }
    public function perform(){
        Role::where('name', 'admin')->first()->users->map(function($admin){
            Mail::to($admin->email)->send(new TicketStoreMail($this->ticket));
        });
    }
}