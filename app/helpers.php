<?php

function apiResponse($data = null, $message = null, $status = 200, $paginator = null)
{
    $arr = [
        'data' => $data,
        'message' => $message
    ];
    $paginator ? $arr['meta'] = [
        'current_page' => $paginator->currentPage(),
        'per_page' => $paginator->perPage(),
        'total' => $paginator->total(),
        'last_page' => $paginator->lastPage(),
    ] : null;
    return response()->json($arr, $status);
}
function getImportanceBadgeClass($importance)
{
    $class = '';
    switch ($importance) {
        case 'urgent':
            $class = 'badge bg-danger';
            break;
        case 'important':
            $class = 'badge bg-warning';
            break;
        case 'normal':
            $class = 'badge bg-info';
            break;
    }
    return $class;
}

function getStatusBadgeClass($status)
{
    $class = '';
    switch ($status) {
        case 'pending':
            $class = 'badge bg-secondary';
            break;
        case 'in_progress':
            $class = 'badge bg-warning';
            break;
        case 'closed':
            $class = 'badge bg-success';
            break;
    }
    return $class;
}

function getStatusText($status)
{
    $text = '';
    switch ($status) {
        case 'pending':
            $text = 'Pending';
            break;
        case 'in_progress':
            $text = 'In Progress';
            break;
        case 'closed':
            $text = 'Closed';
            break;
    }
    return $text;
}