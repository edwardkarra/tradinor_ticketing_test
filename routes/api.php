<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\TicketController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Models\Role;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('/login', [AuthController::class, 'login']);
Route::group(['middleware' => ['auth:sanctum', 'admin']], function(){
    Route::get('/tickets', [TicketController::class, 'index']);
    Route::put('/tickets/{ticket}/change-status', [TicketController::class, 'changeStatus'])->name('tickets.change_status');
});