<?php
namespace App\Repository;

use App\Models\Ticket;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface TicketRepositoryInterface
{
   public function getPaginatedTickets($search = null, $sort = 'date', $direction = 1) : LengthAwarePaginator;
   public function createTicket(array $fields): Ticket;
   public function createHistoryRecord(Ticket $ticket, string $old_status): Ticket;
   public function changeStatus(Ticket $ticket, string $new_status): Ticket;
}