<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TicketStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string', 'max:50'],
            'description' => ['required', 'string', 'max:4000'],
            'attachment' => ['nullable', 'file', 'mimes:jpg,png,jpeg,pdf', 'max:20480'],
            'importance' => ['required', 'in:Urgent,Important,Normal'],
            'name' => ['required', 'string', 'max:50'],
            'phone_number' => ['nullable', 'string', 'min:4', 'max:13'],
            'email' => ['required_without:phone_number', 'string', 'max:255'],
        ];
    }
}
