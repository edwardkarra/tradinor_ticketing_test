<?php
namespace App\Http\Actions\Ticket;
use App\Http\Actions\ActionInterface;
use App\Mail\TicketStoreMail;
use App\Models\Ticket;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;
class TicketStoreNotifyAction implements ActionInterface{
    private TicketStoreNotifyStrategy $strategy;
    public function __construct(TicketStoreNotifyStrategy $strategy){
        $this->strategy = $strategy;
    }

    public static function make(TicketStoreNotifyStrategy $strategy){
        return new self($strategy);
    }
    public function perform(){
        $this->strategy->perform();
    }
}