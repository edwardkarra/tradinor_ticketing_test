<?php

namespace App\Http\Controllers;

use App\Http\Actions\Ticket\TicketClosedMailStrategy;
use App\Http\Actions\Ticket\TicketClosedNotifyAction;
use App\Http\Actions\Ticket\TicketStoreMailAction;
use App\Http\Actions\Ticket\TicketStoreMailStrategy;
use App\Http\Actions\Ticket\TicketStoreNotifyAction;
use App\Http\Requests\TicketChangeStatusRequest;
use App\Http\Requests\TicketShowRequest;
use App\Http\Requests\TicketStoreRequest;
use App\Mail\TicketClosedMail;
use App\Models\Ticket;
use App\Repository\TicketCommentRepositoryInterface;
use App\Repository\TicketRepositoryInterface;
use App\Repository\TStatusHistoryRepositoryInterface;
use Illuminate\Http\Request;
use Mail;

class TicketController extends Controller
{
    private $ticketRepository;
    private $statusHistoryRepository;
    private $commentRepository;
    public function __construct(TicketRepositoryInterface $ticketRepository,TStatusHistoryRepositoryInterface $statusHistoryRepository, TicketCommentRepositoryInterface $commentRepository) {
        $this->ticketRepository = $ticketRepository;
        $this->statusHistoryRepository = $statusHistoryRepository;
        $this->commentRepository = $commentRepository;
    }

    public function index(Request $request)
    {
        $search = $request->input('search');
        $sort = $request->input('sort', 'created_at');
        $direction = $request->input('direction', -1);
        $tickets = $this->ticketRepository->getPaginatedTickets($search, $sort, $direction);

        return view('tickets.index', ['tickets' => $tickets]);
    }

    public function show(TicketShowRequest $request, Ticket $ticket){
        return view('tickets.show', [
            'ticket' => $ticket,
            'status_history' => $this->statusHistoryRepository->getOfTicket($ticket),
            'comments' => $this->commentRepository->getOfTicket($ticket),
        ]);
    }
    public function store(TicketStoreRequest $request)
    {
        $ticket = $this->ticketRepository->createTicket($request->validated());

        $request->session()->flash('success', 'Thanks for submitting your ticket!');
        $request->session()->flash('link', route('tickets.show', ['ticket' => $ticket->id]));
        TicketStoreNotifyAction::make(new TicketStoreMailStrategy($ticket))->perform();
        return redirect($request->input('redirect'));
    }
    
    public function changeStatus(TicketChangeStatusRequest $request, Ticket $ticket){
        $old_status = $ticket->status;
        $this->ticketRepository->changeStatus($ticket, $request->validated()['new_status']);
        $this->ticketRepository->createHistoryRecord($ticket->refresh(), $old_status);

        $ticket->status == 'closed' ? TicketClosedNotifyAction::make(new TicketClosedMailStrategy($ticket))->perform() : null;

        return redirect($request->input('redirect'));
    } 
}