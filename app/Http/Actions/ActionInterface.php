<?php
namespace App\Http\Actions;

interface ActionInterface {
    /**
     * Perform the action.
     *
     * @return mixed
     */
    public function Perform();
}